package com.gitlab.croclabs;

import com.gitlab.croclabs.aspect.db.holder.DataSourceHolder;
import com.gitlab.croclabs.aspect.db.mocked.MockedDataSource;
import lombok.extern.java.Log;

@Log
public class Main {
	public static void main(String[] args) {
		DataSourceHolder.set(new MockedDataSource());

		log.info("Hello world!");
		new Test().test();
		new Test().test("1", 2);
		log.info(new Test().getLong(1).orElse(-1L) + "");
		log.info(new Test().getUpdate(1, "man").orElse(false) + "");
		log.info(new Test().transaction());
		log.info(new Test().transaction());
	}
}