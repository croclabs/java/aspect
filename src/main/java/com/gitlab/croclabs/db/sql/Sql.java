package com.gitlab.croclabs.db.sql;

import com.gitlab.croclabs.aspect.db.data.SqlValue;
import com.gitlab.croclabs.aspect.db.holder.ConnectionHolder;
import com.gitlab.croclabs.aspect.db.holder.DataSourceHolder;
import com.gitlab.croclabs.aspect.db.sql.SqlResult;

import java.sql.*;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public final class Sql {
	private Sql() {
		super();
	}

	public static <T> Optional<T> connect(ConnectionHandler<T> handler) {
		Optional<T> r = Optional.empty();
		boolean thread = ConnectionHolder.CON.get() != null;

		try (Connection con = Objects.requireNonNullElse(ConnectionHolder.CON.get(), DataSourceHolder.get().getConnection())) {
			con.setAutoCommit(false);

			if (!thread) {
				ConnectionHolder.CON.set(con);
			}

			r = Optional.ofNullable(handler.handle(con));

			if (!thread) {
				con.commit();
				ConnectionHolder.CON.remove();
			}
		} catch (Throwable e) {
			// TODO
		}

		return r;
	}

	public static <T> T statement(String sql, StatementHandler<T> handler, Object... args) throws Throwable {
		try (PreparedStatement ps = ConnectionHolder.CON.get().prepareStatement(sql)) {
			AtomicInteger i = new AtomicInteger(1);

			for (Object arg : args) {
				if (arg instanceof SqlValue) {
					Object value = ((SqlValue) arg).getValue();
					JDBCType type = ((SqlValue) arg).getType();
					ps.setObject(i.getAndIncrement(), value, type.getVendorTypeNumber());
				} else {
					ps.setObject(i.getAndIncrement(), arg);
				}
			}

			return handler.handle(ps);
		}
	}

	public static <T> T query(PreparedStatement ps, QueryHandler<T> handler) throws Throwable {
		try (ResultSet rs = ps.executeQuery()) {
			return handler.handle(rs);
		}
	}

	public static <T> T update(PreparedStatement ps, UpdateHandler<T> handler) throws Throwable {
		return handler.handle(ps.executeUpdate());
	}

	public static <T> SqlResult<T> executeQuery(QueryHandler<T> handler) {
		return SqlResult.query(handler);
	}

	public static <T> SqlResult<T> executeUpdate(UpdateHandler<T> handler) {
		return SqlResult.update(handler);
	}

	@FunctionalInterface
	public interface ConnectionHandler<T> {
		T handle(Connection con) throws Throwable;
	}

	@FunctionalInterface
	public interface StatementHandler<T> {
		T handle(PreparedStatement ps) throws Throwable;
	}

	@FunctionalInterface
	public interface QueryHandler<T> {
		T handle(ResultSet rs) throws Throwable;
	}

	@FunctionalInterface
	public interface UpdateHandler<T> {
		T handle(int rows) throws Throwable;
	}
}
