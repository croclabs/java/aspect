package com.gitlab.croclabs.aspect.logging;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Logged {
	boolean start() default true;
	boolean args() default true;
	boolean request() default true;
	boolean response() default true;
	boolean session() default true;
	boolean end() default true;
}
