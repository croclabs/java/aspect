package com.gitlab.croclabs.aspect.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class DefaultLoggedAspect extends AbstractLoggedAspect {
	@Override
	@Around("all() && @annotation(log)")
	public Object logged(ProceedingJoinPoint pjp, Logged log) throws Throwable {
		Object r;

		if (log.start()) logMethodStart(pjp);
		if (log.args()) logMethodArgs(pjp);

		r = pjp.proceed();

		if (log.end()) logMethodEnd(pjp);

		return r;
	}
}
