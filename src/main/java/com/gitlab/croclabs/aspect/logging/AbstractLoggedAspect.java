package com.gitlab.croclabs.aspect.logging;

import com.gitlab.croclabs.aspect.abstracts.AbstractAspect;
import lombok.extern.java.Log;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.Arrays;

@Log
public abstract class AbstractLoggedAspect extends AbstractAspect {
	protected void logMethodStart(ProceedingJoinPoint pjp) {
		log.info("calling:\t\t\t" + ((MethodSignature) pjp.getSignature()).getMethod().getName());
	}

	protected void logMethodArgs(ProceedingJoinPoint pjp) {
		log.info("With parameters:\t" + Arrays.toString(pjp.getArgs()));
	}

	protected void logMethodEnd(ProceedingJoinPoint pjp) {
		log.info("calling:\t\t\t" + ((MethodSignature) pjp.getSignature()).getMethod().getName());
	}

	abstract public Object logged(ProceedingJoinPoint pjp, Logged log) throws Throwable;
}
