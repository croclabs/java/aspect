package com.gitlab.croclabs.aspect.db.transaction;

import com.gitlab.croclabs.aspect.abstracts.AbstractAspect;
import com.gitlab.croclabs.aspect.db.holder.ConnectionHolder;
import com.gitlab.croclabs.aspect.db.holder.DataSourceHolder;
import lombok.extern.java.Log;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.sql.Connection;

@Log
@Aspect
public class TransactionAspect extends AbstractAspect {

	@Around("all() && @annotation(transaction)")
	public Object transaction(ProceedingJoinPoint pjp, Transaction transaction) {
		Object r = null;
		try (Connection con = DataSourceHolder.get().getConnection()) {
			ConnectionHolder.CON.set(con);
			r = pjp.proceed();
			ConnectionHolder.CON.remove();
		} catch (Throwable e) {
			log.severe(e.getMessage());
		}
		return r;
	}
}
