package com.gitlab.croclabs.aspect.db.sql;

import com.gitlab.croclabs.db.sql.Sql.QueryHandler;
import com.gitlab.croclabs.db.sql.Sql.UpdateHandler;

import java.sql.ResultSet;
import java.util.Optional;

public class SqlResult<T> {
	private QueryHandler<T> handler = null;
	private UpdateHandler<T> updateHandler = null;
	private T result = null;

	private SqlResult() {
		super();
	}

	private SqlResult(QueryHandler<T> handler) {
		this.handler = handler;
	}

	private SqlResult(UpdateHandler<T> handler) {
		this.updateHandler = handler;
	}

	private SqlResult(QueryHandler<T> handler, UpdateHandler<T> updateHandler) {
		this.handler = handler;
		this.updateHandler = updateHandler;
	}

	public static <K> SqlResult<K> query(QueryHandler<K> handler) {
		return new SqlResult<>(handler);
	}

	public static <K> SqlResult<K> update(UpdateHandler<K> handler) {
		return new SqlResult<>(handler);
	}

	public static <K> SqlResult<K> empty() {
		return new SqlResult<>(rs -> null, rows -> null);
	}

	public T get() {
		return result;
	}

	public Optional<T> optional() {
		return Optional.ofNullable(result);
	}

	public T orElse(T other) {
		return optional().orElse(other);
	}

	protected SqlResult<T> query(ResultSet rs) throws Throwable {
		result = handler.handle(rs);
		return this;
	}

	protected SqlResult<T> update(int rows) throws Throwable {
		result = updateHandler.handle(rows);
		return this;
	}
}
