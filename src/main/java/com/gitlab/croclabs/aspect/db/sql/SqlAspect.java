package com.gitlab.croclabs.aspect.db.sql;

import com.gitlab.croclabs.aspect.abstracts.AbstractAspect;
import com.gitlab.croclabs.db.sql.Sql;
import lombok.extern.java.Log;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;

@Log
@Aspect
@SuppressWarnings("unchecked")
public class SqlAspect extends AbstractAspect {
	@Around("all() && @annotation(query)")
	public Object update(ProceedingJoinPoint pjp, SqlUpdate query) {
		return doQuery(pjp, query.query(), query.logged(),
				ps -> ((SqlResult<Object>) pjp.proceed()).update(ps.executeUpdate()));
	}

	@Around("all() && @annotation(query)")
	public Object query(ProceedingJoinPoint pjp, SqlQuery query) {
		return doQuery(pjp, query.query(), query.logged(), ps -> {
			try (ResultSet rs = ps.executeQuery()) {
				return ((SqlResult<Object>) pjp.proceed()).query(rs);
			}
		});
	}

	private SqlResult<Object> doQuery(ProceedingJoinPoint pjp, String query, boolean logged, SqlAction sqlAction) {
		if (logged) {
			log.info(query
					.replaceAll("\r\n", " ")
					.replaceAll("\n", " ")
					.replaceAll("\t", " ") + " | " + Arrays.toString(pjp.getArgs()));
		}

		return Sql.connect(con -> {
			if (logged) {
				log.info(con.toString());
			}

			return Sql.statement(query, ps -> {
				if (logged) {
					log.info(ps.toString());
				}

				return sqlAction.action(ps);
			}, pjp.getArgs());
		}).orElse(SqlResult.empty());
	}

	@FunctionalInterface
	private interface SqlAction {
		SqlResult<Object> action(PreparedStatement ps) throws Throwable;
	}
}
