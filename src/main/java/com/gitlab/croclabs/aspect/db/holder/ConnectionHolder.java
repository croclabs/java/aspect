package com.gitlab.croclabs.aspect.db.holder;

import com.gitlab.croclabs.aspect.db.data.ThreadConnection;

public final class ConnectionHolder {
	public static final ThreadConnection CON = new ThreadConnection();

	private ConnectionHolder() {
		super();
	}
}
