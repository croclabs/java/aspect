package com.gitlab.croclabs.aspect.db.holder;

import javax.sql.DataSource;

public final class DataSourceHolder {
	private static DataSource dataSource;

	private DataSourceHolder() {
		super();
	}

	public static void set(DataSource dataSource) {
		if (DataSourceHolder.dataSource == null) DataSourceHolder.dataSource = dataSource;
	}

	public static DataSource get() {
		return dataSource;
	}
}
