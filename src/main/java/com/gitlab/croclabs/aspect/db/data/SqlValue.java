package com.gitlab.croclabs.aspect.db.data;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.sql.JDBCType;

@Data
@Builder
@RequiredArgsConstructor
public class SqlValue {
	private final Object value;
	private final JDBCType type;
}
