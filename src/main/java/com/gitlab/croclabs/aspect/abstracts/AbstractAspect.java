package com.gitlab.croclabs.aspect.abstracts;

import org.aspectj.lang.annotation.Pointcut;

public abstract class AbstractAspect {
	@Pointcut("execution(public * *(..))")
	public void allPublic() { }

	@Pointcut("execution(protected * *(..))")
	public void allProtected() { }

	@Pointcut("execution(private * *(..))")
	public void allPrivate() { }

	@Pointcut("(allPublic() || allProtected() || allPrivate())")
	public void all() { }
}
