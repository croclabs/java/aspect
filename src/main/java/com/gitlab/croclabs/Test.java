package com.gitlab.croclabs;

import com.gitlab.croclabs.aspect.db.sql.SqlQuery;
import com.gitlab.croclabs.aspect.db.sql.SqlResult;
import com.gitlab.croclabs.aspect.db.sql.SqlUpdate;
import com.gitlab.croclabs.aspect.db.transaction.Transaction;
import com.gitlab.croclabs.aspect.logging.Logged;
import com.gitlab.croclabs.db.sql.Sql;
import lombok.extern.java.Log;

@Log
public class Test {
	@Logged
	public void test() {
		protectedTest();
		privateTest();
		System.out.println("test");
	}

	@Logged
	public void test(String one, int two) {
		log.info(one + " " + two);
	}

	@Logged
	protected void protectedTest() {
		log.info("test");
	}

	@Logged
	private void privateTest() {
		log.info("test");
	}

	@SqlQuery(
			query = """
					SELECT * FROM boi
					""",
			logged = true
	)
	public SqlResult<Long> getLong(long id) {
		return Sql.executeQuery(rs -> rs.getLong(""));
	}

	@SqlUpdate(
			query = """
					SELECT * FROM boi
					""",
			logged = true
	)
	public SqlResult<Boolean> getUpdate(long id, String man) {
		return Sql.executeUpdate(rows -> true);
	}

	@Transaction
	public String transaction() {
		this.getLong(1);
		this.getUpdate(2, "transaction");
		return "you know";
	}
}
